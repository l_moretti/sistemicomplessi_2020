from ant_simulator.server import server
import asyncio # Se non lo metto non funziona commentate se non necessario

# Anche questa riga è necessaria per me, commentate se non necessaria
#asyncio.set_event_loop_policy(asyncio.WindowsSelectorEventLoopPolicy())

server.launch()
