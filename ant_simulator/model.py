from mesa import Agent, Model
from mesa.time import RandomActivation,BaseScheduler
from mesa.space import MultiGrid
from ant_simulator.agents import Ant,Feromone,Food,Nest,Wall
from mesa.datacollection import DataCollector
import csv
import tcod
import math 
import os

# TODO: TUNARE
# TODO: grafichini

def get_num_workers_agents(model):
    """ return number of workers agents"""
      
    workers_agents = [a for a in model.schedule_ants.agents if a.food_location != None]

    n_workers= len(workers_agents)
    model.current_step+=1

    if model.reached_half_colony == False and n_workers >= (model.number_of_agents/2):
        model.reached_half_colony = True
        f = open("RaccoltaDati/info.txt","a+")
        f.write(f"mondo {model.width}x{model.height}: metà colonia workers: {model.current_step} \n ")
        f.close()
    if model.ended_scenario == False and model.reached_half_colony == True and n_workers == 0:
        model.ended_scenario = True
        f = open("RaccoltaDati/info.txt","a+")
        f.write(f"mondo {model.width}x{model.height}: fine scenario: {model.current_step} \n ")
        f.close()
        #model.stop() try to stop model

    return n_workers


def get_num_explorers_agents(model):
    """return number of explorators agents"""

    explorers_agents = [a for a in model.schedule_ants.agents if a.food_location == None]
    return len(explorers_agents)

def get_num_food_pieces(model):
    """return number of food pieces"""

    food_pieces = sum(a.units for a in model.schedule_food.agents if a.units > 0)
    return food_pieces

def get_num_food_sources(model):
    """return number of food sources"""

    food_sources = [a for a in model.schedule_food.agents if a.units > 0]
    return len(food_sources)

class AntModel(Model):
    """
    The Ant Model
    """
    
    def __init__(self, number_of_agents,width,height,fade_time,
                food_cells,food_quantity,max_feromone_level,spawns_for_step,
                max_strong_level,max_feromone_boost, max_strong_boost_add,
                enable_strong_feromone,read_from_csv):
        """
        Ant Model constructor.

        Arguments:

        int number_of_agents: maximum number of ant agents in the model.
        int ant_counter: current number of ant agents in the model.
        int width: grid width.
        int height: grid height.
        int fade_time: steps the feromone takes to fade
        int food_cells: food cells number
        int food_quantity: how much units of food each food cell has
        int max_feromone_level: max ants feromone level 
        int spawns_for_step: how many ants spawn each step
        bool enable_strong_feromone: enabled/disabled strong feromone
        bool read_from_csv: enabled/disabled template
        bool reached_half_colony: Enable writing on file
        bool ended_scenario: indicates if a scenario is ended
        """
        self.number_of_agents = number_of_agents

        self.grid = MultiGrid(width,height,False) # La griglia non è toroidale

        self.reached_half_colony = False
        self.ended_scenario = False
        self.current_step = 0

        self.width = width
        self.height = height
        self.map = tcod.map.Map(self.width,self.height,order="F") # Creo mappa per pathfinding
        self.map.walkable[:] = True # Setto tutte le celle come trasparenti
        self.map.transparent[:] = True # Setto tutte le celle camminabili

        self.max_feromone_level = max_feromone_level
        self.max_strong_level = max_strong_level
        self.fade_time = fade_time

        #Schedulers
        self.schedule_ants = RandomActivation(self)
        self.schedule_feromone = BaseScheduler(self)
        self.schedule_food = BaseScheduler(self)

        self.max_feromone_boost= max_feromone_boost
        self.max_strong_boost_add= max_strong_boost_add

        self.enable_strong_feromone=enable_strong_feromone
        self.food_cells = food_cells # Numero di celle contenenti cibo
        self.food_quantity = food_quantity # Unità di cibo per cella

        self.spawns_for_step=spawns_for_step

        self.counter = 0 # Contatore id

        self.ant_counter = 0 # numero attuale di formiche
        self.read_from_csv = read_from_csv

        # Definisco la locazione del nido, ovvero il punto di spawn
        # delle formiche
        # Se sto usando un template il nido sarà al centro
        if read_from_csv:
            nest_pos_x = round(self.grid.width/2)
            nest_pos_y = round(self.grid.height/2)
        else:
            nest_pos_x = self.random.randrange(self.grid.width)
            nest_pos_y = self.random.randrange(self.grid.height)

        nest = Nest(self.counter,self)
        self.grid.place_agent(nest,(nest_pos_x,nest_pos_y))
        self.nest_pos = (nest_pos_x,nest_pos_y)
        self.counter += 1

        # see datacollector functions above
        self.datacollector = DataCollector(
            model_reporters={
                "Explorers" : get_num_explorers_agents,
                "Workers": get_num_workers_agents,
                "Food pieces": get_num_food_pieces,
                "Food sources": get_num_food_sources,
            }
        )

        # Piazzo cella cibo, per ora niente scheduler visto che deve solo subire
        # Controllo che non piazzi una cella cibo sopra il nido
        if read_from_csv:
            with open("ant_simulator/template.csv") as csv_file:
                csv_reader = csv.reader(csv_file, delimiter=',')
                line_count = 0
                foods = []
                for row in csv_reader:
                    #leggo il food schema dal template
                    if line_count == 0:
                        food_scheme = row[0]
                        #print(row[0])
                        if food_scheme == "4 corners":
                            length = math.floor(self.grid.width/4)
                            pos1 = nest_pos_x + length
                            pos2 = nest_pos_y - length
                            foods = [[pos1,pos1],[pos1,pos2],[pos2,pos1],[pos2,pos2]]

                        elif food_scheme == "4 axis":
                            length = math.floor(self.grid.width/4)
                            pos1 = nest_pos_x + length
                            pos2 = nest_pos_y - length
                            foods = [[nest_pos_x,pos1],[nest_pos_x,pos2],[pos1,nest_pos_y],[pos2,nest_pos_y]]

                        else:
                            print("incorrect food schema name in template")
                        
                        line_count += 1

                    else: #leggo il wall schema dal template e aggiungo passo per passo i muri

                        wall = Wall(self.counter,self)

                        list_coefficents= [-1,1]

                        for coeff_x in list_coefficents:
                            for coeff_y in list_coefficents:

                                x = math.floor(coeff_x*float(row[0])*self.grid.width/2) + nest_pos_x
                                y = math.floor(coeff_y*float(row[1])*self.grid.height/2) + nest_pos_y
                                pos_occupated= []

                                if (x,y) != (nest_pos_x,nest_pos_y) and not [x,y] in pos_occupated:
                                    pos_occupated.append([x,y])
                                    self.grid.place_agent(wall,(x,y))
                                    self.map.transparent[x,y] = False
                                    self.map.walkable[x,y] = False
                                    self.counter += 1

                                line_count += 1
                        
                #aggiungo le caselle cibo
                for el in foods:
                    food = Food(self.counter,self)
                    x = el[0]
                    y = el[1]
                    cellmates = self.grid.get_cell_list_contents((x,y))

                    # Controllo che non ci sia già il nido o celle bloccanti
                    if (x,y) != (nest_pos_x,nest_pos_y) and not any(type(agent) == Wall for agent in cellmates):
                        self.grid.place_agent(food,(x,y))
                        self.schedule_food.add(food)
                        self.counter += 1

        else:
            for i in range(self.food_cells):
                food = Food(self.counter,self)
                while True:
                    x = self.random.randrange(self.grid.width)
                    y = self.random.randrange(self.grid.height)

                    if (x,y) != (nest_pos_x,nest_pos_y):
                        self.grid.place_agent(food,(x,y))
                        self.schedule_food.add(food)
                        self.counter += 1
                        break

        # Inizializzo il livello di feromone per ogni cella (a 0) e le aggiungo 
        # allo scheduler per far decrementare la potenza del feromone con il tempo
        for i in range(width):
            for j in range(height):
                feromone_level =  Feromone(self.counter,self)
                self.grid.place_agent(feromone_level,(i,j))
                self.counter += 1
                self.schedule_feromone.add(feromone_level)


        self.running = True
        self.datacollector.collect(self)

    def step(self):
        """
        Step function that defines what the model does at each step
        """
        self.schedule_ants.step()
        self.schedule_feromone.step()
        self.schedule_food.step()
        
        #multi spawn
        if self.ant_counter < self.number_of_agents:
            for i in range(1,self.spawns_for_step):
                self.SpawnAgent(self.nest_pos)

        # collect data
        self.datacollector.collect(self)

    def SpawnAgent(self,pos):
        """
        Spawn an ant agent at the given position.

        Arguments:
        int pos_x: x coordinate.
    
        int pos_y: y coordinate.
        """
        # controllo che non siano già stati creati tutti gli agenti
        if(self.ant_counter < self.number_of_agents):
            current_pos = (pos[0], pos[1])
            agent = Ant(self.counter, self, current_pos)
            self.counter += 1
            self.ant_counter += 1
                
            # Creo gli agenti e li aggiungo allo scheduler
            self.grid.place_agent(agent, (pos[0], pos[1]))
            self.schedule_ants.add(agent)
