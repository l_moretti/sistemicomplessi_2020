from mesa import Agent, Model
from mesa.time import RandomActivation
from mesa.space import MultiGrid

import numpy
import math
from numpy.random import choice
import tcod

class Ant(Agent):
    """
    Ant agent to simulate an ant.
    """
    def __init__(self,id,model,old_pos):
        """
        Ant Agent constructor.

        Arguments:

        int id: unique identifier.
        Model model: model where the agent operate.
        tuple old_pos: old position of the ant.
        """

        super().__init__(id,model)

        self.food_units = 0 # number of food units transported during the simulation
        self.old_pos = old_pos
        self.got_food = False # Sta andando al cibo o al nido?
        self.food_location = None
        self.path = None # Cammino verso il nido
        self.next_move =  None
        
    
    def move(self):
        """
        Movement function.
        """
        if self.food_location == None:
            # Controllo il vicinato per vedere se c'è cibo
            neighbors = self.model.grid.get_neighbors(self.pos,True,False)
            for agent in neighbors:
                if type(agent) is Food:
                    self.food_location = agent.pos
                    break
        
        # Non ha trovato cibo e sta esplorando
        if self.food_location == None:
            new_pos = self.random_movement()

        # Sta facendo avanti e indietro per portare cibo
        else:
            # Sto trasportando cibo
            if self.got_food:
                if self.pos == self.model.nest_pos:
                    # Lascio il cibo al nido
                    self.got_food = False
                    self.path.reverse()
                    self.next_move = 1
                    new_pos = self.path[0]
                else:
                    # Sto andando al nido
                    new_pos = self.path[self.next_move]
                    self.next_move += 1
            else:
                # Non sto trasportando cibo

                # Mi trovo sulla casella cibo, lo prendo
                if self.pos == self.food_location:

                    found = False
                    cellmates = self.model.grid.get_cell_list_contents([self.pos])

                    for agent in cellmates:
                        if type(agent) is Food:

                            found = True
                            first_time = False
                            self.got_food = True
                            agent.units -= 1
                            self.food_units += 1

                            if self.path == None:
                                self.compute_a_star(self.model.nest_pos[0],self.model.nest_pos[1])
                                first_time = True

                            if agent.units == 0:
                                agent.kill()

                            break
                    if found:
                        # C'è ancora del cibo

                        # Controllo se non è la prima volta che passo sul cibo
                        # In tal caso devo invertire la lista
                        if not first_time: 
                            self.path.reverse()
                            self.next_move = 0

                        new_pos = self.path[self.next_move]
                        self.next_move += 1

                    else:
                        # Il cibo è finito
                        self.food_location = None
                        self.old_pos = self.pos
                        self.path = None
                        new_pos = self.random_movement()
                # Sto andando a prendere il cibo
                else:
                    delete = False
                    
                    if self.path == None:
                        self.compute_a_star(self.food_location[0],self.food_location[1])
                        delete = True

                    new_pos = self.path[self.next_move]
                    self.next_move += 1

                    if delete: self.path = None

        # Prima di muoversi rilascia il feromone
        self.lay_feromone()
        self.model.grid.move_agent(self, new_pos)
        
    def lay_feromone(self):
        """
        Increase feromone level in current position by one.
        """

        cellmates = self.model.grid.get_cell_list_contents([self.pos])

        for agent in cellmates:
            if type(agent) is Feromone:
                if self.food_location != None and self.model.enable_strong_feromone:
                    # Devo rilasciare quello forte
                    if agent.strong == False:
                        agent.strong = True # Se non è forte lo resetto a forte
                        agent.level = 1

                    elif agent.level < self.model.max_strong_level:
                        agent.level += 1
                    
                    agent.timer = agent.fade_time
                    
                else:
                    # Devo rilasciare quello debole
                    # Se c'è quello forte non rilascio nulla perchè sto esplorando
                    if agent.level < self.model.max_feromone_level and agent.strong == False:
                        agent.level += 1
                        agent.timer = agent.fade_time # Reset del timer
                break
    
    def random_movement(self):
        """
        Random movement that keeps track of past direction
        """
        if(self.old_pos == self.pos):
            #Prima mossa, salvo la posizione attuale e scelgo a caso una direzione
            possible_steps = self.model.grid.get_neighborhood(
                self.pos,
                moore = False,
                include_center = False)
            self.old_pos = self.pos
            new_pos = self.random.choice(possible_steps)

            #Controllo che la nuova posizione non sia un muro
            invalid_coord = True
            while(invalid_coord):
                correct_new_position = False
                wall_not_found = True

                if(new_pos[0]>=0 and new_pos[0]< self.model.width and new_pos[1]>=0 and new_pos[1]< self.model.height):
                    correct_new_position = True
                    
                    cell_new_pos = self.model.grid.get_cell_list_contents([new_pos])
                    for agent in cell_new_pos:
                        if type(agent) is Wall:
                            wall_not_found = False                

                #la nuova posizione è valida
                if correct_new_position and wall_not_found:
                    invalid_coord = False
                else:
                    #rimuovo il vecchio possible step e ricalcolo
                    possible_steps.remove(new_pos)
                    new_pos = self.random.choice(possible_steps)

        else:
            #Mossa generica, calcolo una nuova direzione tenendo conto dell'ultima mossa fatta
            diff = tuple(map(lambda x, y: x - y, self.pos, self.old_pos))
            #ad ogni direzione della mossa associo le direzioni future possibili e quanto sono probabili
            if diff==(0,-1): candidates = [(0,-1),(-1,-1),(1,-1),(1,0),(-1,0)]
            elif diff == (0,1): candidates = [(0,1),(1,1),(-1,1),(1,0),(-1,0)]
            elif diff == (1,0): candidates = [(1,0),(1,-1),(1,1),(0,1),(0,-1)]
            elif diff == (-1,0): candidates = [(-1,0),(-1,-1),(-1,1),(0,1),(0,-1)]
            elif diff == (1,1): candidates = [(1,1),(1,0),(0,1),(-1,1),(1,-1)]
            elif diff == (-1,1): candidates = [(-1,1),(-1,0),(0,1),(-1,-1),(1,1)]
            elif diff == (-1,-1): candidates = [(-1,-1),(-1,0),(0,-1),(-1,1),(1,-1)]
            elif diff == (1,-1): candidates = [(1,-1),(1,0),(0,-1),(1,1),(-1,-1)]
            indexes = [0,1,2,3,4]
            
            prior_distribution = [0.55, 0.15, 0.15, 0.075, 0.075]
            #print("prior p:" , prior_distribution)
            p_boost = []
            # scorro le possibili nuove posizioni candidate
            for el in candidates:
                pos = tuple(map(lambda x, y: x + y, self.pos, el))
                # controllo che non esca dalla griglia
                if(pos[0]>=0 and pos[0]< self.model.width and pos[1]>=0 and pos[1]< self.model.height):
                    # ottengo gli agenti nella cella che sto esaminando
                    cellmates = self.model.grid.get_cell_list_contents([pos])
                    level = 0
                    boost = 0 
                    for agent in cellmates:
                        if type(agent) is Feromone:
                            level = agent.level
                            if agent.strong == True:
                                #calcolo boost feromone forte
                                boost = self.model.max_feromone_boost + \
                                                                    (
                                                                    self.model.max_strong_boost_add * \
                                                                    level/self.model.max_strong_level
                                                                    )
                                #print(f' strong {boost}')
                            else:
                                #calcolo boost feromone debole
                                boost = self.model.max_feromone_boost * \
                                         level/self.model.max_feromone_level
                                #print(f' debole {boost}')

                    p_boost.append(boost)
                else:
                    p_boost.append(0)

            p_distribution = [sum(x) for x in zip(prior_distribution, p_boost)]
            p_distribution = normalize_p_distribution(p_distribution)
            #print("final p:" , p_distribution)

            invalid_coord = True
            while(invalid_coord): #controllo che non vadano oltre i limiti della board
                draw = choice(indexes, p=p_distribution)
                new_pos = tuple(map(lambda x, y: x + y, self.pos, candidates[draw]))

                #controllo che la new_pos sia dentro la board
                correct_new_position = False
                wall_not_found = True

                if(new_pos[0]>=0 and new_pos[0]< self.model.width and new_pos[1]>=0 and new_pos[1]< self.model.height):
                    correct_new_position = True
                    
                    cell_new_pos = self.model.grid.get_cell_list_contents([new_pos])
                    for agent in cell_new_pos:
                        if type(agent) is Wall:
                            wall_not_found = False                

                #la nuova posizione è valida
                if correct_new_position and wall_not_found:
                    invalid_coord = False
                else:
                    #testo le altre possibili posizioni
                    if numpy.count_nonzero(p_distribution) >= 2:
                        p_distribution[draw] = 0
                        p_distribution = normalize_p_distribution(p_distribution)
                        
                    else:
                        #sono in un angolo o in un vicolo cieco di ostacoli quindi faccio dietro-front
                        new_pos = self.old_pos
                        invalid_coord = False

                self.old_pos = self.pos
        
        return new_pos
    
    def compute_a_star(self,target_x,target_y):
        """
        Compute a path from original position to target position updating
        the self.path attribute
        """
        if not self.model.read_from_csv:
            return
        
        my_path = tcod.path_new_using_map(self.model.map)

        (x,y) = self.pos

        tcod.path_compute(my_path,x,y,target_x,target_y)

        # Controllo che non sia vuoto
        if not tcod.path_is_empty(my_path):
            self.path = my_path.get_path(x,y,target_x,target_y)
            self.path.insert(0,(x,y))
            self.next_move = 1
            #print(self.path)
            tcod.path_delete(my_path)

    def move_towards(self,target_x,target_y):
        """
        Basic moving function towards a target cell returns the next cell
        to move to.

        Arguments:

        int target_x: target x coord
        int target_y: target y coord
        """

        dx = target_x - self.pos[0]
        dy = target_y - self.pos[1]

        distance = math.sqrt(dx ** 2 + dy ** 2)

        dx = int(round(dx / distance))
        dy = int(round(dy / distance))

        new_pos_x = self.pos[0] + dx
        new_pos_y = self.pos[1] + dy

        return (new_pos_x,new_pos_y)

    def step(self):
        """
        At each step of the model the agent will execute this function.
        """
        self.move()

def normalize_p_distribution(p_distribution):
    tot = numpy.sum(p_distribution)
    new_p = []
    for el in p_distribution:
        new_p.append(el/tot)
    return new_p

class Feromone(Agent):
    """
    Feromone agent to keep track of the cell feromone level.
    """

    #TODO: feromone potente
    #      aggiuingere booleano e nel modello mettere un max_strong per liv
    #      massimo dello strong

    def __init__(self,id,model):
        """
            Feromone agent constructor.

            int id: unique identifier.
            Model model: model where the agent operate.
        """

        super().__init__(id,model)
        self.level = 0
        self.strong = False
        self.fade_time = model.fade_time
        self.timer = self.fade_time
        
    def step(self):
        """
        At each step the agent will execute this function.
        """
        # Quando il timer arriva a 0 perde un'unità
        if self.level > 0:
            self.timer -= 1

            if self.timer == 0:
                if self.strong == True and self.level > 1:
                    self.level -= 2
                else:
                    self.level -= 1
                
                # Se il livello raggiunge lo 0 ed è strong allora devo tornare ai valori standard
                if self.level == 0 and self.strong == True: 
                    self.strong = False

                self.timer = self.fade_time

class Food(Agent):
    """
    Food agent to simulate the presence of food in the cell.
    """

    def __init__(self,id,model):
        """
        Constructor.

        int id: unique identifier.
        Model model: model where the agent operate.
        """

        super().__init__(id,model)
        self.units = model.food_quantity # Quantità di cibo
    
    def step(self):
        """
        At each step the agent will execute this function.
        """

    def kill(self):
        """
        Destroy the agent.
        """
        self.model.grid.remove_agent(self)

class Nest(Agent):
    """
    Nest agent where the ants spawn
    """

    def __init__(self,id,model):
        """
        Constructor.

        int id: unique identifier.
        Model model: model where the agent operate.
        """

        super().__init__(id,model)

class Wall(Agent):
    """
    Wall agent to simulate an obstacle
    """

    def __init__(self,id,model):
        """
        Constructor
        """

        super().__init__(id,model)
