f = open("template.csv","a+")

x = 0.23
y = 0.39
while(x < 0.43):
    f.write(f"{round(x,5)},{round(y,5)}\n")
    f.write(f"{round(x,5)},{round(y+0.00125,5)}\n")
    f.write(f"{round(x,5)},{round(y-0.00125,5)}\n")
    f.write(f"{round(x+0.00125,5)},{round(y,5)}\n")
    f.write(f"{round(x-0.00125,5)},{round(y,5)}\n")
    x+=0.0025
    y-=0.0025

f.close()