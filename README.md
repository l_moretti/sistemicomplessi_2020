# Ant pheromone simulator


## How to start the project

From the main directory of the project use the following code to start the simulator's UI: <br/>
` python3 run.py `<br/>
<br/>
From the main directory of the project use the following code to start the simulator in batch mode: <br/>
` python3 batch_run.py `

Authors: Bertini Simone, Moretti Luca, Fabbris Giorgio <br/>
Year: 2020 <br/>
