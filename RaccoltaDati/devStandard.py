from os import walk
from sklearn.preprocessing import minmax_scale
import statistics
from numpy import mean, absolute

def averageAbsoluteDeviation(data):

    # Find mean value of the sample
    M = mean(data)
    sum = 0

    # Calculate mean absolute deviation
    for i in range(len(data)):
        dev = absolute(data[i] - M)
        sum = sum + round(dev,2)
    print(sum/len(data))
    return sum/len(data)


for (dirpath, dirnames, filenames) in walk('ConMuri/'):
    for filename in filenames:
        file=filename.split('.')[0]
        partiFile=file.split('-')
        grandezzaMondo = partiFile[2][4:]
        fine_o_meta = partiFile[2][:4]
        f = open('ConMuri/'+filename,"r")
        lines = f.readlines() 
        listaValori=[]
        for line in lines[1:]:
            if line != '' and line != ' \n':
                if line.__contains__('-'):
                    break           
                listaValori.append(int(line))    
        normalizedList = minmax_scale(listaValori, feature_range=(0, 1), axis=0, copy=True)
        stDev = statistics.stdev(normalizedList)
        aad = averageAbsoluteDeviation(normalizedList)

        if fine_o_meta =='fine':
            dataset = open('dataset-devStandardFinire.csv',"a+")
        else:
            dataset = open('dataset-devStandardMeta.csv',"a+")
        
        dataset.write(f'{grandezzaMondo},{1},{partiFile[3]},{round(stDev,5)},{round(aad,5)} \n')


        



