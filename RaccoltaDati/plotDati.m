dataset = readtable('dataset-formiche.csv');
NoFeromone= dataset(dataset{:,2} == 0,:);
SiFeromone= dataset(dataset{:,2} == 1,:);
NoFeromoneSiMuro= NoFeromone(NoFeromone{:,3} ==1,:);
NoFeromoneNoMuro= NoFeromone(NoFeromone{:,3} ==0,:);
SiFeromoneSiMuro= SiFeromone(SiFeromone{:,3} ==1,:);
SiFeromoneNoMuro= SiFeromone(SiFeromone{:,3} ==0,:);


f1=figure;
hold on
title({'Comparazione modelli 1/2 feromoni','senza ostacoli per finire scenario'})
xlabel('Lunghezza lato griglia')
ylabel('nr. step')
plot(NoFeromoneNoMuro{:,1},NoFeromoneNoMuro{:,5},'-o','DisplayName','1 Feromone','color',[.9 .1 .1] )

plot(SiFeromoneNoMuro{:,1},SiFeromoneNoMuro{:,5},'-o','DisplayName','2 Feromoni','color',[.1 .9 .1] )

legend('Location','northwest')
saveas(gcf,'Grafici/noMuriFine.png')
%drawnow
hold off

f2=figure;
hold on

title({'Comparazione modelli 1/2 feromoni','senza ostacoli per 50% formiche workers'})
xlabel('Lunghezza lato griglia')
ylabel('nr. step')
plot(NoFeromoneNoMuro{:,1},NoFeromoneNoMuro{:,4},'-o','DisplayName','1 Feromone','color',[.9 .1 .1] )

plot(SiFeromoneNoMuro{:,1},SiFeromoneNoMuro{:,4},'-o','DisplayName','2 Feromoni','color',[.1 .9 .1] )
plot(SiFeromoneNoMuro{:,1},SiFeromoneNoMuro{:,4}.*2,'--','DisplayName','2 Feromoni * 2','color',[.1 .1 .8] )

legend('Location','northwest')

drawnow
saveas(gcf,'Grafici/noMuriMeta.png')
hold off

f3=figure;
hold on
title({'Comparazione modelli 1/2 feromoni','con ostacoli per finire scenario'})
xlabel('Lunghezza lato griglia')
ylabel('nr. step')
plot(NoFeromoneSiMuro{:,1},NoFeromoneSiMuro{:,5},'-o','DisplayName','1 Feromone','color',[.9 .1 .1] )

plot(SiFeromoneSiMuro{:,1},SiFeromoneSiMuro{:,5},'-o','DisplayName','2 Feromoni','color',[.1 .9 .1] )

legend('Location','northwest')

%drawnow
hold off

f4=figure;
hold on

title({'Comparazione modelli 1/2 feromoni','con ostacoli per raggiungere il 50% di formiche workers'})

plot(NoFeromoneSiMuro{:,1},NoFeromoneSiMuro{:,4},'-o','DisplayName','1 Feromone','color',[.9 .1 .1] )
plot(SiFeromoneSiMuro{:,1},SiFeromoneSiMuro{:,4}.*2,'--','DisplayName','2 Feromoni * 2','color',[.1 .1 .8] )
plot(SiFeromoneSiMuro{:,1},SiFeromoneSiMuro{:,4},'-o','DisplayName','2 Feromoni','color',[.1 .9 .1] )

xlabel('Lunghezza lato griglia')
ylabel('nr. step')
legend('Location','northwest')

saveas(gcf,'Grafici/siMuriMeta.png')
%drawnow
hold off


f5=figure;
hold on


title({'Comparazione modelli 1/2 feromoni','con ostacoli per finire scenario'})

plot(NoFeromoneSiMuro{:,1},NoFeromoneSiMuro{:,5},'-o','DisplayName','1 Feromone','color',[.9 .1 .1] )

plot(SiFeromoneSiMuro{:,1},SiFeromoneSiMuro{:,5},'-o','DisplayName','2 Feromoni','color',[.1 .9 .1] )
xlabel('Lunghezza lato griglia')
ylabel('nr. step')
legend('Location','northwest')

saveas(gcf,'Grafici/siMuriFinire.png')
%drawnow
hold off


f6=figure;
hold on

title({'Comparazione modelli 1/2 feromoni','con e senza ostacoli per finire scenario'})

plot(NoFeromoneSiMuro{:,1},NoFeromoneSiMuro{:,5},'-o','DisplayName','1 Feromone con ostacoli','color',[.9 .1 .1] )

plot(SiFeromoneSiMuro{:,1},SiFeromoneSiMuro{:,5},'-o','DisplayName','2 Feromoni con ostacoli','color',[.1 .9 .1] )

plot(NoFeromoneNoMuro{:,1},NoFeromoneNoMuro{:,5},'-o','DisplayName','1 Feromone senza ostacoli','color',[.9 .1 .9] )

plot(SiFeromoneNoMuro{:,1},SiFeromoneNoMuro{:,5},'-o','DisplayName','2 Feromoni senza ostacoli','color',[.1 .1 .9] )
xlabel('Lunghezza lato griglia')
ylabel('nr. step')


legend('Location','northwest')

saveas(gcf,'Grafici/comparaSiMuriNoMuri1_2feromoniFinire.png')
%drawnow
hold off

f61=figure;
hold on

title({'Comparazione modelli 1/2 feromoni','con e senza ostacoli per 50% formiche workers'})

plot(NoFeromoneSiMuro{:,1},NoFeromoneSiMuro{:,4},'-o','DisplayName','1 Feromone con ostacoli','color',[.9 .1 .1] )

plot(SiFeromoneSiMuro{:,1},SiFeromoneSiMuro{:,4},'-o','DisplayName','2 Feromoni con ostacoli','color',[.1 .9 .1] )

plot(NoFeromoneNoMuro{:,1},NoFeromoneNoMuro{:,4},'-o','DisplayName','1 Feromone senza ostacoli','color',[.9 .1 .9] )

plot(SiFeromoneNoMuro{:,1},SiFeromoneNoMuro{:,4},'-o','DisplayName','2 Feromoni senza ostacoli','color',[.1 .1 .9] )
xlabel('Lunghezza lato griglia')
ylabel('nr. step')


legend('Location','northwest')

saveas(gcf,'Grafici/comparaSiMuriNoMuri1_2feromonimeta.png')
%drawnow
hold off

f11=figure;
hold on

title({'Comparazione modelli a 1 feromone','con e senza ostacoli per finire scenario'})

plot(NoFeromoneSiMuro{:,1},NoFeromoneSiMuro{:,5},'-o','DisplayName','Con ostacoli','color',[.9 .1 .1] )

plot(NoFeromoneNoMuro{:,1},NoFeromoneNoMuro{:,5},'-o','DisplayName','Senza ostacoli','color',[.9 .1 .9] )

xlabel('Lunghezza lato griglia')
ylabel('nr. step')

legend('Location','northwest')

saveas(gcf,'Grafici/comparaSiMuriNoMuriFinire1Feromone.png')
%drawnow
hold off


f7=figure;
hold on

title({'Comparazione modelli 2 feromoni','con e senza ostacoli per finire scenario'})

plot(SiFeromoneSiMuro{:,1},SiFeromoneSiMuro{:,5},'-o','DisplayName','Con ostacoli','color',[.1 .9 .1] )

plot(SiFeromoneNoMuro{:,1},SiFeromoneNoMuro{:,5},'-o','DisplayName','Senza ostacoli','color',[.1 .1 .9] )

xlabel('Lunghezza lato griglia')
ylabel('nr. step')
%INCREMENTO CIRCA COSTANTE DEL 10%

legend('Location','northwest')

saveas(gcf,'Grafici/comparaSiMuriNoMuriFinire2Feromoni.png')
%drawnow
hold off


f8=figure;
hold on

title({'Decremento % nr. step usando 2 feromoni','rispetto 1 solo per finire lo scenario'})


f(1)= plot(NoFeromoneSiMuro{:,1},((SiFeromoneNoMuro{:,5}-NoFeromoneNoMuro{:,5})./NoFeromoneNoMuro{:,5})*100,'-o','DisplayName','Diminuzione % 2 feromoni senza ostacoli','color',[.9 .1 .1] );
f(2)= plot (NoFeromoneSiMuro{:,1},((SiFeromoneSiMuro{:,5}-NoFeromoneSiMuro{:,5})./NoFeromoneSiMuro{:,5})*100,'-o','DisplayName','Diminuzione % 2 feromoni con ostacoli','color',[.1 .9 .1] );
yl = yline(-5,'--','-5%');
y2 = yline(-25,'--','-25%');
y1.LabelHorizontalAlignment = 'left';
y2.LabelHorizontalAlignment = 'left';

legend(f([1 2]),'Senza ostacoli', 'Con ostacoli'); %legend of 1,3

ylim([-50 30])
%xlim([20 300])
xlabel('Lunghezza lato griglia')
ylabel('% decremento')

%INCREMENTO CIRCA COSTANTE DEL 10%

legend('Location','northwest')

saveas(gcf,'Grafici/compara1e2FeromoniIncrementoPercentuale.png')
%drawnow
hold off

f9=figure;
hold on

title({'Decremento % nr. step causato dall''assenza di ostacoli','per finire lo scenario'})

f(1)= plot(SiFeromoneSiMuro{:,1},((SiFeromoneNoMuro{:,5}-SiFeromoneSiMuro{:,5})./SiFeromoneSiMuro{:,5})*100,'-o','DisplayName','Diminuzione % 2 feromoni senza ostacoli','color',[.9 .1 .1] );
f(2)= plot (NoFeromoneSiMuro{:,1},((NoFeromoneNoMuro{:,5}-NoFeromoneSiMuro{:,5})./NoFeromoneSiMuro{:,5})*100,'-o','DisplayName','Diminuzione % 2 feromoni con ostacoli','color',[.1 .9 .1] );
yl = yline(-10,'--','-10%');
y2 = yline(-35,'--','-35%');
y1.LabelHorizontalAlignment = 'left';
y2.LabelHorizontalAlignment = 'left';

legend(f([1 2]),'2 feromoni', '1 feromone'); %legend of 1,3
                
ylim([-50 30])
%xlim([20 300])
xlabel('Lunghezza lato griglia')
ylabel('% decremento')

legend('Location','northwest')

saveas(gcf,'Grafici/comparaMuroNoMuroIncrementoPercentuale.png')
%drawnow
hold off


f28=figure;
hold on

title({'Decremento % nr. step usando 2 feromoni','rispetto 1 solo per raggiungere il 50% di formiche workers'})


f(1)= plot(NoFeromoneSiMuro{:,1},((SiFeromoneNoMuro{:,4}-NoFeromoneNoMuro{:,4})./NoFeromoneNoMuro{:,4})*100,'-o','DisplayName','Diminuzione % 2 feromoni senza ostacoli','color',[.9 .1 .1] );
f(2)= plot (NoFeromoneSiMuro{:,1},((SiFeromoneSiMuro{:,4}-NoFeromoneSiMuro{:,4})./NoFeromoneSiMuro{:,4})*100,'-o','DisplayName','Diminuzione % 2 feromoni con ostacoli','color',[.1 .9 .1] );
yl = yline(-30,'--','-30%');
y2 = yline(-60,'--','-60%');
y1.LabelHorizontalAlignment = 'left';
y2.LabelHorizontalAlignment = 'left';

legend(f([1 2]),'senza ostacoli', 'con ostacoli'); %legend of 1,3

ylim([-80 0])
%xlim([20 300])
xlabel('Lunghezza lato griglia')
ylabel('% decremento')

%INCREMENTO CIRCA COSTANTE DEL 10%

legend('Location','northwest')

saveas(gcf,'Grafici/compara1e2FeromoniIncrementoPercentualeMeta.png')
%drawnow
hold off

f29=figure;
hold on

title({'Decremento % nr. step causato dall''assenza di ostacoli','per raggiungere il 50% delle formiche workers'})

f(1)= plot(SiFeromoneSiMuro{:,1},((SiFeromoneNoMuro{:,4}-SiFeromoneSiMuro{:,4})./SiFeromoneSiMuro{:,4})*100,'-o','DisplayName','Diminuzione % 2 feromoni senza ostacoli','color',[.9 .1 .1] );
f(2)= plot (NoFeromoneSiMuro{:,1},((NoFeromoneNoMuro{:,4}-NoFeromoneSiMuro{:,4})./NoFeromoneSiMuro{:,4})*100,'-o','DisplayName','Diminuzione % 2 feromoni con ostacoli','color',[.1 .9 .1] );
yl = yline(-10,'--','-10%');
y2 = yline(-40,'--','-40%');
y1.LabelHorizontalAlignment = 'left';
y2.LabelHorizontalAlignment = 'left';

legend(f([1 2]),'2 feromoni', '1 feromone'); %legend of 1,3
                
ylim([-80 30])
%xlim([20 300])
xlabel('Lunghezza lato griglia')
ylabel('% decremento')

legend('Location','northwest')

saveas(gcf,'Grafici/comparaMuroNoMuroIncrementoPercentualeMeta.png')
%drawnow
hold off

datasetDevFinire = readtable('dataset-devStandardFinire.csv');
datasetDevMeta = readtable('dataset-devStandardMeta.csv');

dv_NoFeromoneFinire=datasetDevFinire(datasetDevFinire{:,3} == 0,:);
dv_SiFeromoneFinire=datasetDevFinire(datasetDevFinire{:,3} == 1,:);

dv_NoFeromoneNoMuroFinire=sortrows(dv_NoFeromoneFinire(dv_NoFeromoneFinire{:,2} == 0,:),1);
dv_NoFeromoneSiMuroFinire=sortrows(dv_NoFeromoneFinire(dv_NoFeromoneFinire{:,2} == 1,:),1);
dv_SiFeromoneNoMuroFinire=sortrows(dv_SiFeromoneFinire(dv_SiFeromoneFinire{:,2} == 0,:),1);
dv_SiFeromoneSiMuroFinire=sortrows(dv_SiFeromoneFinire(dv_SiFeromoneFinire{:,2} == 1,:),1);

dv_NoFeromoneMeta=datasetDevMeta(datasetDevMeta{:,3} == 0,:);
dv_SiFeromoneMeta=datasetDevMeta(datasetDevMeta{:,3} == 1,:);

dv_NoFeromoneNoMuroMeta=sortrows(dv_NoFeromoneMeta(dv_NoFeromoneMeta{:,2} == 0,:),1);
dv_NoFeromoneSiMuroMeta=sortrows(dv_NoFeromoneMeta(dv_NoFeromoneMeta{:,2} == 1,:),1);
dv_SiFeromoneNoMuroMeta=sortrows(dv_SiFeromoneMeta(dv_SiFeromoneMeta{:,2} == 0,:),1);
dv_SiFeromoneSiMuroMeta=sortrows(dv_SiFeromoneMeta(dv_SiFeromoneMeta{:,2} == 1,:),1);

f40=figure;
hold on


title({'Deviazione standard risultati simulazioni','scalati in [0,1] per finire lo scenario'})

f(1)= plot(dv_NoFeromoneNoMuroFinire{:,1},dv_NoFeromoneNoMuroFinire{:,4},'-o','DisplayName','1 feromone senza ostacoli','color',[.9 .1 .1] );
f(2)= plot (dv_NoFeromoneSiMuroFinire{:,1},dv_NoFeromoneSiMuroFinire{:,4},'-o','DisplayName','1 feromone con ostacoli','color',[.1 .9 .1] );
f(3)= plot(dv_SiFeromoneNoMuroFinire{:,1},dv_SiFeromoneNoMuroFinire{:,4},'-o','DisplayName','2 feromoni senza ostacoli','color',[.1 .1 .9] );
f(4)= plot (dv_SiFeromoneSiMuroFinire{:,1},dv_SiFeromoneSiMuroFinire{:,4},'-o','DisplayName',' 2 feromoni con ostacoli','color',[.9 .9 .1] );

ylim([0 0.8])
                
xlabel('Lunghezza lato griglia')
ylabel('Deviazione standard')

legend('Location','northwest')

saveas(gcf,'Grafici/devStandard_comparaMuroNoMuroIncrementoPercentualeFinire.png')
%drawnow
hold off

f41=figure;
hold on


title({'Deviazione standard risultati simulazioni','scalati in [0,1] per raggiungere il 50% formiche workers'})

f(1)= plot(dv_NoFeromoneNoMuroMeta{:,1},dv_NoFeromoneNoMuroMeta{:,4},'-o','DisplayName','1 feromone senza ostacoli','color',[.9 .1 .1] );
f(2)= plot (dv_NoFeromoneSiMuroMeta{:,1},dv_NoFeromoneSiMuroMeta{:,4},'-o','DisplayName','1 feromone con ostacoli','color',[.1 .9 .1] );
f(3)= plot(dv_SiFeromoneNoMuroMeta{:,1},dv_SiFeromoneNoMuroMeta{:,4},'-o','DisplayName','2 feromoni senza ostacoli','color',[.1 .1 .9] );
f(4)= plot (dv_SiFeromoneSiMuroMeta{:,1},dv_SiFeromoneSiMuroMeta{:,4},'-o','DisplayName',' 2 feromoni con ostacoli','color',[.9 .9 .1] );

ylim([0 0.8])
                
xlabel('Lunghezza lato griglia')
ylabel('Deviazione standard')

legend('Location','northwest')

saveas(gcf,'Grafici/devStandard_comparaMuroNoMuroIncrementoPercentualeMeta.png')
%drawnow
hold off


f43=figure;
hold on


title({'Mean Absolute Deviation risultati simulazioni','scalati in [0,1] per finire lo scenario'})

f(1)= plot(dv_NoFeromoneNoMuroFinire{:,1},dv_NoFeromoneNoMuroFinire{:,5},'-o','DisplayName','1 feromone senza ostacoli','color',[.9 .1 .1] );
f(2)= plot (dv_NoFeromoneSiMuroFinire{:,1},dv_NoFeromoneSiMuroFinire{:,5},'-o','DisplayName','1 feromone con ostacoli','color',[.1 .9 .1] );
f(3)= plot(dv_SiFeromoneNoMuroFinire{:,1},dv_SiFeromoneNoMuroFinire{:,5},'-o','DisplayName','2 feromoni senza ostacoli','color',[.1 .1 .9] );
f(4)= plot (dv_SiFeromoneSiMuroFinire{:,1},dv_SiFeromoneSiMuroFinire{:,5},'-o','DisplayName',' 2 feromoni con ostacoli','color',[.9 .9 .1] );

ylim([0 0.8])
                
xlabel('Lunghezza lato griglia')
ylabel('MAD')

legend('Location','northwest')

saveas(gcf,'Grafici/aad_comparaMuroNoMuroIncrementoPercentualeFinire.png')
%drawnow
hold off

f44=figure;
hold on


title({'Mean Absolute Deviation risultati simulazioni','scalati in [0,1] per raggiungere il 50% formiche workers'})

f(1)= plot(dv_NoFeromoneNoMuroMeta{:,1},dv_NoFeromoneNoMuroMeta{:,5},'-o','DisplayName','1 feromone senza ostacoli','color',[.9 .1 .1] );
f(2)= plot (dv_NoFeromoneSiMuroMeta{:,1},dv_NoFeromoneSiMuroMeta{:,5},'-o','DisplayName','1 feromone con ostacoli','color',[.1 .9 .1] );
f(3)= plot(dv_SiFeromoneNoMuroMeta{:,1},dv_SiFeromoneNoMuroMeta{:,5},'-o','DisplayName','2 feromoni senza ostacoli','color',[.1 .1 .9] );
f(4)= plot (dv_SiFeromoneSiMuroMeta{:,1},dv_SiFeromoneSiMuroMeta{:,5},'-o','DisplayName',' 2 feromoni con ostacoli','color',[.9 .9 .1] );

ylim([0 0.8])
                
xlabel('Lunghezza lato griglia')
ylabel('MAD')

legend('Location','northwest')

saveas(gcf,'Grafici/aad_comparaMuroNoMuroIncrementoPercentualeMeta.png')
%drawnow
hold off



f47=figure;
hold on


title({'Coefficente di variazione risultati simulazioni','per finire lo scenario'})

f(1)= plot(dv_NoFeromoneNoMuroFinire{:,1},dv_NoFeromoneNoMuroFinire{:,6},'-o','DisplayName','1 feromone senza ostacoli','color',[.9 .1 .1] );
f(2)= plot (dv_NoFeromoneSiMuroFinire{:,1},dv_NoFeromoneSiMuroFinire{:,6},'-o','DisplayName','1 feromone con ostacoli','color',[.1 .9 .1] );
f(3)= plot(dv_SiFeromoneNoMuroFinire{:,1},dv_SiFeromoneNoMuroFinire{:,6},'-o','DisplayName','2 feromoni senza ostacoli','color',[.1 .1 .9] );
f(4)= plot (dv_SiFeromoneSiMuroFinire{:,1},dv_SiFeromoneSiMuroFinire{:,6},'-o','DisplayName',' 2 feromoni con ostacoli','color',[.9 .9 .1] );

ylim([0 0.3])
                
xlabel('Lunghezza lato griglia')
ylabel('Coefficente di variazione')

legend('Location','northwest')

saveas(gcf,'Grafici/devmediaFinire.png')
%drawnow
hold off

f46=figure;
hold on


title({'Coefficente di variazione risultati simulazioni','per raggiungere il 50% formiche workers'})

f(1)= plot(dv_NoFeromoneNoMuroMeta{:,1},dv_NoFeromoneNoMuroMeta{:,6},'-o','DisplayName','1 feromone senza ostacoli','color',[.9 .1 .1] );
f(2)= plot (dv_NoFeromoneSiMuroMeta{:,1},dv_NoFeromoneSiMuroMeta{:,6},'-o','DisplayName','1 feromone con ostacoli','color',[.1 .9 .1] );
f(3)= plot(dv_SiFeromoneNoMuroMeta{:,1},dv_SiFeromoneNoMuroMeta{:,6},'-o','DisplayName','2 feromoni senza ostacoli','color',[.1 .1 .9] );
f(4)= plot (dv_SiFeromoneSiMuroMeta{:,1},dv_SiFeromoneSiMuroMeta{:,6},'-o','DisplayName',' 2 feromoni con ostacoli','color',[.9 .9 .1] );

ylim([0 0.3])
                
xlabel('Lunghezza lato griglia')
ylabel('Coefficente di variazione')

legend('Location','northwest')

saveas(gcf,'Grafici/DevmediaMeta.png')
%drawnow
hold off



